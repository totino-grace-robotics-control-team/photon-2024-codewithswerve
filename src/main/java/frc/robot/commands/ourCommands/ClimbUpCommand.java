package frc.robot.commands.ourCommands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ClimbSubsystem.ClimbSS;

public class ClimbUpCommand extends Command {
     private final ClimbSS m_climb;

    public ClimbUpCommand(ClimbSS subsystem) {
         System.out.println("intakeRollers (command): Constructed");
        m_climb = subsystem;
      //   Use addRequirements() here to declare subsystem dependencies.
        addRequirements(subsystem);
    }
    
    @Override
    public void initialize() {
      
    }
    @Override
    public void execute() {

      m_climb.posClimb();

    }
    @Override
    public void end(boolean interrupted) {

      m_climb.stopClimb();

    }
    @Override
    public boolean isFinished() {
        return false;
    }

}

