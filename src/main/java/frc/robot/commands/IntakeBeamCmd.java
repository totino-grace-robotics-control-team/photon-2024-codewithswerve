package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeAndShootSS;

public class IntakeBeamCmd extends Command{
    private final IntakeAndShootSS m_IntakeAndShoot;

    public IntakeBeamCmd(IntakeAndShootSS subsystem) {
        // System.out.println("intakeRollers (command): Constructed");
        m_IntakeAndShoot = subsystem;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(subsystem);
    }
    @Override
    public void initialize() {
        //set the motor speed here
        m_IntakeAndShoot.startingIntake();
    }

    @Override
    public void execute() {
        //nothing's here
    }
     @Override
    public boolean isFinished() {
        //stop motors here
       return m_IntakeAndShoot.isNoteLoaded();
        
    }

    @Override
    public void end(boolean interrupted) {
        //stop motor here
        m_IntakeAndShoot.stoppingIntake();

    }

}
