/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.cscore.UsbCamera;
import edu.wpi.first.cscore.VideoSink;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;


public class CameraManager extends SubsystemBase {
	public int m_currentCamera = -1;
	private VideoSink m_video;
	private UsbCamera m_camera1;
	private UsbCamera m_camera2;
	//private UsbCamera m_camera3;

	//Create two USB Cameras and a CameraServer, then switch the source camera to change the video feed
	public CameraManager() {
	  super();
	  
	  //Cameras
	  m_camera1 = CameraServer.startAutomaticCapture(0);
	  m_camera2 = CameraServer.startAutomaticCapture(1); 
	  //m_camera3 = CameraServer.startAutomaticCapture(2);

	  //CameraServer
	  m_video = CameraServer.getServer();

	  //Configure the cameras
	  m_camera1.setFPS(Constants.visionConstants.kCameraFPS);
	  m_camera2.setFPS(Constants.visionConstants.kCameraFPS);
	//  m_camera3.setFPS(Constants.visionConstants.kCameraFPS); //Probably belongs in Constants
  
	  //Initialize the camera to start
	  if (m_currentCamera == -1) {
		m_video.setSource(m_camera2);
		shutdownCamera(m_camera1);
		m_currentCamera = 2;
	  }
	}
  
	//This method flips between the cameras
	public void toggleCamera() { //This could be done better with a switch, and an enum...
		System.out.println("current camera: " + m_currentCamera);
		if (m_currentCamera == 1) {
			shutdownCamera(m_camera1);
			reinitCamera(m_camera2);
			m_video.setSource(m_camera2);
			m_currentCamera = 2;
			System.out.println("camera switched to 2!");
			//SmartDashboard.putNumber("toggle camera", m_currentCamera);
		} else if (m_currentCamera == 2) {
			shutdownCamera(m_camera2);
			reinitCamera(m_camera1);
			m_video.setSource(m_camera1);
			m_currentCamera = 1;
			System.out.println("camera switched to 1!");
		//	SmartDashboard.putNumber("toggle camera", m_currentCamera);


		} 
	}

	//Shuts down whichever camera you pass it
	private void shutdownCamera(UsbCamera camera) {
	  camera.setResolution(0, 0);
	  //camera.setFPS(0);
	}
  
	//Turns on whichever camera you pass it
	private void reinitCamera(UsbCamera camera) {
	  camera.setResolution(320, 240); //belongs in Constants
	  //camera.setFPS(20);
	}
}