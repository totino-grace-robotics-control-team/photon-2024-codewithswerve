package frc.robot.vision;

import edu.wpi.first.networktables.DoubleSubscriber;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.TimestampedDouble;

public class NetSubscriber {

    // network table topic subscription 
   DoubleSubscriber dblSub;

   // to hold the last value obtained, to check for changes
   double m_prev; 

   NetworkTable m_NetTable;
   private static String TABLE_NAME="VisionData";
   private static String DBL_TOPIC_NAME="Topic1";
   private static double DEFAULT_TOPIC_VALUE = 0.0;


   public DoubleSubscriber setupNetTable() {
       m_NetTable = NetworkTableInstance.getDefault().getTable(TABLE_NAME);
       DoubleSubscriber sub = m_NetTable.getDoubleTopic(DBL_TOPIC_NAME).subscribe(DEFAULT_TOPIC_VALUE);
       return sub;
   }


  public NetSubscriber() {
    dblSub = setupNetTable();

    // // subscribe options may be specified using PubSubOption
    // dblSub =
    //     dblTopic.subscribe(0.0, PubSubOption.keepDuplicates(true), PubSubOption.pollStorage(10));

    // // subscribeEx provides the options of using a custom type string.
    // // Using a custom type string for types other than raw and string is not recommended.
    // dblSub = dblTopic.subscribeEx("double", 0.0);
  }


  /**
   * call this at some interval to look for published values
   */
  public void periodic() {

      // get() can be used with simple change detection to the previous value
      double value = dblSub.get();
      if (value != m_prev) {
          m_prev = value; // save previous value
          System.out.println("X changed value: " + value);
      }

      // readQueueValues() provides all value changes since the last call;
      // this way it's not possible to miss a change by polling too slowly
      for (double iterVal : dblSub.readQueueValues()) {
          System.out.println("X changed value: " + iterVal);
      }

      // readQueue() is similar to readQueueValues(), but provides timestamps
      // for each change as well
      for (TimestampedDouble tsValue : dblSub.readQueue()) {
          System.out.println("X changed value: " + tsValue.value + " at local time " + tsValue.timestamp);
      }

      // simple get of most recent value; if no value has been published,
      // returns the default value passed to the subscribe() function
    //   double val = dblSub.get();

      // get the most recent value; if no value has been published, returns
      // the passed-in default value
      double val = dblSub.get(-1.0);

      // subscribers also implement the appropriate Supplier interface, e.g.
      // DoubleSupplier
    //   double val = dblSub.getAsDouble();

      // get the most recent value, along with its timestamp
    //   TimestampedDouble tsVal = dblSub.getAtomic();

      // read all value changes since the last call to readQueue/readQueueValues
      // readQueue() returns timestamps; readQueueValues() does not.
    //   TimestampedDouble[] tsUpdates = dblSub.readQueue();
    //   double[] valUpdates = dblSub.readQueueValues();
  }

  // often not required in robot code, unless this class doesn't exist for
  // the lifetime of the entire robot program, in which case close() needs to be
  // called to stop subscribing
  public void close() {
    // stop subscribing
    dblSub.close();
  }
}    

